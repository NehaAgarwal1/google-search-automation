package pages;

import framework.Wait;
import org.openqa.selenium.By;

public class searchPage extends Wait {

    private static final By search = By.name("q");
    private static final By submitARequest = By.className("gNO89b");

    public static void searchWord(String word){
        driver.findElement(search).sendKeys(word);
        driver.findElement(submitARequest).submit();
    }
}