package framework;

import java.util.logging.Logger;

public class LoggerClass extends Wait {

    private static Logger logging;

    public Logger setInstanceOfLogger() {
        logging = Logger.getLogger(this.getClass().getName());
        return logging;
    }

    public static void print(String message) {
        logging.info(message);
    }
}
