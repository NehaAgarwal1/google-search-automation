package framework;

import pages.searchPage;

public class PageFactory extends LoggerClass{
    private static searchPage search;

    public static void setSearch(){
        search = new searchPage();

    }

    public static searchPage getSearch(){
        return search;
    }
}
