package framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;

public class Driver{
    protected static WebDriver driver;
    //WebDriver driver;

    public static WebDriver setInstanceOfDriver() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

//    public static WebDriver setInstanceOfDriver(ChromeOptions option) {
//        driver = new ChromeDriver(option);
//        return driver;
//    }
//    public static WebDriver headlessChrome(){
//        ChromeOptions option = new ChromeOptions();
//        option.addArguments("--headless");
//        driver = setInstanceOfDriver(option);
//        return driver;
//    }


}