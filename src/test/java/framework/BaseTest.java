package framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.logging.Logger;

public class BaseTest extends PageFactory {
    private static final String url = "https://www.google.com";
    WebDriver driver;
    WebDriverWait wait;
    Logger logging;
    @BeforeSuite
    public void setup throws Exceptions() {
        driver = setInstanceOfDriver();
        wait = setInstanceOfWait();
        logging = setInstanceOfLogger();
//        driver = headlessChrome();
        PageFactory.setSearch();
        getUrl();
        //captureScreenShot("GoogleSearch"+java.util.Calendar.getInstance().getTime());
    }


    public void getUrl() {
        driver.get(url);
    }

    @AfterSuite
    public void closeBrowser() {
        driver.quit();
    }

    @AfterMethod
    public void screenshot(ITestResult result)
    {
        String x = "Screenshot"+java.util.Calendar.getInstance().getTime();
        if(ITestResult.FAILURE == result.getStatus())
        {
            Screenshot.captureScreenShot(x);
           // printInfoMessage("Screenshot Captured");
        }
    }
}
