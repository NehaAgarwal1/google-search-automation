package framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Wait extends Driver {
    private static WebDriverWait wait;
    public void setUpWait(){

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    public void waitUntilWordIsTyped(){

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'COMPOSE')]")));
    }

    public static WebDriverWait setInstanceOfWait() {
       wait = new WebDriverWait(driver, 30);

       return wait;

    }
}
